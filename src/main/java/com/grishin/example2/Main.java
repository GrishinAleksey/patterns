package com.grishin.example2;

import com.grishin.example2.brakes.ABS;
import com.grishin.example2.chassis.CarChassis;
import com.grishin.example2.chassis.TruckChassis;
import com.grishin.example2.chassis.VanChassis;
import com.grishin.example2.climate.AirConditioning;
import com.grishin.example2.climate.ClimateControlSystem;
import com.grishin.example2.climate.SeatHeating;
import com.grishin.example2.engine.CarEngine;
import com.grishin.example2.engine.TruckEngine;
import com.grishin.example2.engine.VanEngine;
import com.grishin.example2.steering.PowerSteering;
import com.grishin.example2.steering.StandardSteering;
import com.grishin.example2.vehicle.Car;
import com.grishin.example2.vehicle.Truck;
import com.grishin.example2.vehicle.Van;
import com.grishin.example2.wheels.CarWheels;
import com.grishin.example2.wheels.TruckWheels;
import com.grishin.example2.wheels.VanWheels;

public class Main {

    public static void main(String[] args) {
        // Примените шаблон, благодаря которому не нужно будет заполнять все аргументы такого большого конструктора каждый раз при создании объекта

        Car minimumEquipmentCar = new Car(new CarChassis(), new CarEngine(), new CarWheels(), new StandardSteering(), null, null, null, null);
        Car maximumEquipmentCar = new Car(new CarChassis(), new CarEngine(), new CarWheels(), new PowerSteering(), new ClimateControlSystem(), new AirConditioning(), new SeatHeating(), new ABS());

        Van minimumEquipmentVan = new Van(new VanChassis(), new VanEngine(), new VanWheels(), new StandardSteering(), null, null, null, null);
        Van maximumEquipmentVan = new Van(new VanChassis(), new VanEngine(), new VanWheels(), new PowerSteering(), new ClimateControlSystem(), new AirConditioning(), new SeatHeating(), new ABS());

        Truck minimumEquipmentTruck = new Truck(new TruckChassis(), new TruckEngine(), new TruckWheels(), new StandardSteering(), null, null, null, null);
        Truck maximumEquipmentTruck = new Truck(new TruckChassis(), new TruckEngine(), new TruckWheels(), new PowerSteering(), new ClimateControlSystem(), new AirConditioning(), new SeatHeating(), new ABS());

        // some action with vehicle
    }


}
