package com.grishin.example2.vehicle;

import com.grishin.example2.brakes.ABS;
import com.grishin.example2.chassis.Chassis;
import com.grishin.example2.climate.AirConditioning;
import com.grishin.example2.climate.ClimateControlSystem;
import com.grishin.example2.climate.SeatHeating;
import com.grishin.example2.engine.Engine;
import com.grishin.example2.steering.Steering;
import com.grishin.example2.wheels.Wheels;

public class Van extends Vehicle {


    public Van(Chassis chassis, Engine engine, Wheels wheels, Steering steering, ClimateControlSystem climateControlSystem, AirConditioning airConditioning, SeatHeating seatHeating, ABS abs) {
        super(chassis, engine, wheels, steering, climateControlSystem, airConditioning, seatHeating, abs);
    }

    @Override
    public String toString() {
        return "Van{" +
                "chassis=" + getChassis() +
                ", engine=" + getEngine() +
                ", wheels=" + getWheels() +
                ", steering=" + getSteering() +
                ", climateControlSystem=" + getClimateControlSystem() +
                ", airConditioning=" + getAirConditioning() +
                ", seatHeating=" + getSeatHeating() +
                ", abs=" + getAbs() +
                '}';
    }
}
